const form = document.querySelector('form');

form.addEventListener('submit', (event) => {
  event.preventDefault();

  const username = document.querySelector('#username').value;
  const password = document.querySelector('#password').value;

  const errors = [];

  if (username.length < 6) {
    errors.push('Username must be at least 6 characters long.');
  }

  if (password.length < 10) {
    errors.push('Password must be at least 10 characters long.');
  }

  if (!password.match(/[A-Z]/g) || password.match(/[A-Z]/g).length < 3) {
    errors.push('Password must contain at least 3 uppercase letters.');
  }

  if (!password.match(/[!@*]/g) || password.match(/[!@*]/g).length < 1) {
    errors.push('Password must contain at least 1 special character (!, @, *).');
  }

  if (errors.length > 0) {
    alert(errors.join('\n'));
  } else {
    // Submit the form
  }
});